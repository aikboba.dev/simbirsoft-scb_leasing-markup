const path = require('path');
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin');

module.exports = {
    entry: {
        index: './src/index.js',
        'index-simple': './src/index-simple.js',
    },
    resolve: {
        alias: {
            Fonts: path.resolve(__dirname, 'src/includes/assets/fonts'),
            Images: path.resolve(__dirname, 'src/includes/assets/images'),
        }
    },
    module: {
        rules: [

            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                loader: "file-loader",
                options: {
                    outputPath: 'assets/fonts/'
                }
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loader: "file-loader",
                options: {
                    outputPath: 'assets/images/'
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
                include: path.resolve(__dirname, 'src/includes')
            },
        ]
    },
    devServer: {
        stats: 'errors-only'
    },
    plugins: [
        new HtmlWebpackExternalsPlugin({ // optional plugin: inject cdn
            externals: [
                {
                    module: 'jquery',
                    entry: 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'
                }
            ],
        }),

    ]
};
