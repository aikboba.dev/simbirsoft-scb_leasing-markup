# Элементы формы

## Текстовое поле

#### Начальное состояние

```html
<div class="scb-input-box">
    <label for="input-id">Заголовок поля</label>
    <input type="hidden" id="input-id">
    <input type="text" value="">
    <div class="sub-line error">Место вывода ошибок валидации</div>
</div>
```

#### Ошибка валидации (активация возможна и на других типах полей)

```html
<div class="scb-input-box error-validate">
    <label for="input-id">Стоимость транспортного средства</label>
    <input type="hidden" id="input-id">
    <input type="text" value="">
    <div class="sub-line error">Место вывода ошибок валидации</div>
</div>
```

## Выпадающий список

#### Начальное состояние
```html
<div class="scb-dropdown-box">
    <label for="input-id">Заголовок списка</label>
    <input type="hidden" id="input-id">
    <div class="input-text placeholder">Текст по умолчанию</div>
    <span class="sub-line error">Место вывода ошибок валидации</span>
    <ul class="no-list dropdown-list border">
        <li class="list-item">Пукнт номер 1</li>
        <li class="list-item">Пукнт номер 2</li>
        <li class="list-item">...</li>
    </ul>
</div>
```
#### Список раскрыт
```html
<div class="scb-dropdown-box dropbox-opened">
    <label for="input-id">Заголовок списка</label>
    <input type="hidden" id="input-id">
    <div class="input-text placeholder">Текст по умолчанию</div>
    <span class="sub-line error">Место вывода ошибок валидации</span>
    <ul class="no-list dropdown-list border">
        <li class="list-item">Пукнт номер 1</li>
        <li class="list-item">Пукнт номер 2</li>
        <li class="list-item">...</li>
    </ul>
</div>
```

#### Выбрано значение
```html
<div class="scb-dropdown-box">
    <label for="input-id">Заголовок списка</label>
    <input type="hidden" id="input-id" value="Пукнт номер 2">
    <div class="input-text">Пукнт номер 2</div>
    <span class="sub-line error">Место вывода ошибок валидации</span>
    <ul class="no-list dropdown-list border">
        <li class="list-item">Пукнт номер 1</li>
        <li class="list-item selected">Пукнт номер 2</li>
        <li class="list-item">...</li>
    </ul>
</div>
```

## Ползунок

#### Начальное состояние

```html
<div class="scb-range-box">
    <label for="input-id">Заголовок</label>
    <input type="hidden" id="input-id">
    <span class="input-text">Текущее значение</span>
    <div class="sub-line">
        <span>100 000</span>
        <span>10 млн</span>
    </div>
    <div class="range-line" style="width: [значение в процентах]">
        <div class="range-control"></div>
    </div>
</div>
```

## Триггер (выводится без заголовка)

#### Начальное состояние

```html
<div class="scb-switch-box no-label">
    <span class="input-text">Описание действия триггера</span>
    <div class="scb-trigger">
        <i class="trigger-control"></i>
        <input type="hidden" value="off">
    </div>
    <span class="sub-line error">Место вывода ошибок валидации</span>
</div>
```

#### Активное состояние

```html
<div class="scb-switch-box no-label">
    <span class="input-text">Описание действия триггера</span>
    <div class="scb-trigger on">
        <i class="trigger-control"></i>
        <input type="hidden" value="on">
    </div>
    <span class="sub-line error">Место вывода ошибок валидации</span>
</div>
```
